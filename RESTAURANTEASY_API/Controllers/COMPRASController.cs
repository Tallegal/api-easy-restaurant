﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RESTAURANTEASY_API.Models;

namespace RESTAURANTEASY_API.Controllers
{
    public class COMPRASController : ApiController
    {
        private Entities db = new Entities();

        [Route("api/CrearCompra")]
        [HttpPost]
        public IHttpActionResult CrearCompra(string nombre, string proveedor, string bodega, DateTime fecha, int imponible, int impuesto, int total, string producto, int cantidad, int precio_unitario, int subtotal, int total_detalle)
        {
            var resp = db.SP_CREAR_COMPRA(proveedor, bodega, fecha, imponible, impuesto, total, producto, cantidad, precio_unitario, subtotal, total_detalle);

            return Ok(resp);
        }

        // GET: api/COMPRAS
        public IQueryable<COMPRAS> GetCOMPRAS()
        {
            return db.COMPRAS;
        }

        // GET: api/COMPRAS/5
        [ResponseType(typeof(COMPRAS))]
        public IHttpActionResult GetCOMPRAS(decimal id)
        {
            COMPRAS cOMPRAS = db.COMPRAS.Find(id);
            if (cOMPRAS == null)
            {
                return NotFound();
            }

            return Ok(cOMPRAS);
        }

        // PUT: api/COMPRAS/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCOMPRAS(decimal id, COMPRAS cOMPRAS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cOMPRAS.ID)
            {
                return BadRequest();
            }

            db.Entry(cOMPRAS).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!COMPRASExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/COMPRAS
        [ResponseType(typeof(COMPRAS))]
        public IHttpActionResult PostCOMPRAS(COMPRAS cOMPRAS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.COMPRAS.Add(cOMPRAS);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (COMPRASExists(cOMPRAS.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = cOMPRAS.ID }, cOMPRAS);
        }

        // DELETE: api/COMPRAS/5
        [ResponseType(typeof(COMPRAS))]
        public IHttpActionResult DeleteCOMPRAS(decimal id)
        {
            COMPRAS cOMPRAS = db.COMPRAS.Find(id);
            if (cOMPRAS == null)
            {
                return NotFound();
            }

            db.COMPRAS.Remove(cOMPRAS);
            db.SaveChanges();

            return Ok(cOMPRAS);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool COMPRASExists(decimal id)
        {
            return db.COMPRAS.Count(e => e.ID == id) > 0;
        }
    }
}