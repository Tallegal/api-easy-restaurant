﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RESTAURANTEASY_API.Models;

namespace RESTAURANTEASY_API.Controllers
{
    public class BODEGASController : ApiController
    {
        private Entities db = new Entities();

        [Route("api/UpdateBodega")]
        [HttpPost]
        public IHttpActionResult UpdateBodega(string nombre, string ubicacion, string direccion, string comuna, int id)
        {
            var resp = db.SP_UPDATE_BODEGA(nombre, ubicacion, direccion, comuna, id);
            return Ok(resp);
        }

        [Route("api/CrearBodega")]
        [HttpPost]
        public IHttpActionResult CrearBodega(string nombre, string ubicacion, string direccion, string comuna)
        {
            var resp = db.SP_CREAR_BODEGA(nombre, ubicacion, direccion, comuna);

            return Ok(resp);
        }

        // GET: api/BODEGAS
        public IQueryable<BODEGAS> GetBODEGAS()
        {
            return db.BODEGAS;
        }

        // GET: api/BODEGAS/5
        [ResponseType(typeof(BODEGAS))]
        public IHttpActionResult GetBODEGAS(decimal id)
        {
            BODEGAS bODEGAS = db.BODEGAS.Find(id);
            if (bODEGAS == null)
            {
                return NotFound();
            }

            return Ok(bODEGAS);
        }

        // PUT: api/BODEGAS/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBODEGAS(decimal id, BODEGAS bODEGAS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bODEGAS.ID_BODEGA)
            {
                return BadRequest();
            }

            db.Entry(bODEGAS).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BODEGASExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BODEGAS
        [ResponseType(typeof(BODEGAS))]
        public IHttpActionResult PostBODEGAS(BODEGAS bODEGAS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.BODEGAS.Add(bODEGAS);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (BODEGASExists(bODEGAS.ID_BODEGA))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = bODEGAS.ID_BODEGA }, bODEGAS);
        }

        // DELETE: api/BODEGAS/5
        [ResponseType(typeof(BODEGAS))]
        public IHttpActionResult DeleteBODEGAS(decimal id)
        {
            BODEGAS bODEGAS = db.BODEGAS.Find(id);
            if (bODEGAS == null)
            {
                return NotFound();
            }

            db.BODEGAS.Remove(bODEGAS);
            db.SaveChanges();

            return Ok(bODEGAS);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BODEGASExists(decimal id)
        {
            return db.BODEGAS.Count(e => e.ID_BODEGA == id) > 0;
        }
    }
}