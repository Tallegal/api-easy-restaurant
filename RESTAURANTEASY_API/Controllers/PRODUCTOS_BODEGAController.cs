﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RESTAURANTEASY_API.Models;

namespace RESTAURANTEASY_API.Controllers
{
    public class PRODUCTOS_BODEGAController : ApiController
    {
        private Entities db = new Entities();

        [Route("api/ExisteCodigo")]
        [HttpGet]
        public IHttpActionResult LoginUsuario(string codigo_interno)
        {
            ObjectParameter usu = new ObjectParameter("cONT_USU", typeof(string));
            db.SP_EXISTE_CODIGO_INTERNO(codigo_interno, usu);
            return Ok(int.Parse(usu.Value.ToString()));
        }

        [Route("api/CrearProductoBodega")]
        [HttpPost]
        public IHttpActionResult CrearProductoBodega(string nombre, string descripcion, string codigo_interno, decimal precio, string costo, string unidad_medida, string stock, string venta, string compra, string tipo_producto_bodega)
        {
            var resp = db.SP_CREAR_PRODUCTO_BODEGA(nombre, descripcion, codigo_interno, precio, costo, unidad_medida, stock, venta, compra, tipo_producto_bodega);
            return Ok(resp);
        }

        [Route("api/UpdateProductoBodega")]
        [HttpPost]
        public IHttpActionResult UpdateProductoBodega(int id, string nombre, string descripcion, string codigo_interno, decimal precio, string costo, string unidad_medida, string stock, string venta, string compra, string tipo_producto_bodega)
        {
            var resp = db.SP_UPDATE_PRODUCTO_BODEGA(nombre, descripcion, codigo_interno, precio, costo, unidad_medida, stock, venta, compra, tipo_producto_bodega, id);
            return Ok(resp);
        }

        // GET: api/PRODUCTOS_BODEGA
        public IQueryable<PRODUCTOS_BODEGA> GetPRODUCTOS_BODEGA()
        {
            return db.PRODUCTOS_BODEGA;
        }

        // GET: api/PRODUCTOS_BODEGA/5
        [ResponseType(typeof(PRODUCTOS_BODEGA))]
        public IHttpActionResult GetPRODUCTOS_BODEGA(decimal id)
        {
            PRODUCTOS_BODEGA pRODUCTOS_BODEGA = db.PRODUCTOS_BODEGA.Find(id);
            if (pRODUCTOS_BODEGA == null)
            {
                return NotFound();
            }

            return Ok(pRODUCTOS_BODEGA);
        }

        // PUT: api/PRODUCTOS_BODEGA/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPRODUCTOS_BODEGA(decimal id, PRODUCTOS_BODEGA pRODUCTOS_BODEGA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pRODUCTOS_BODEGA.ID_PRODUCTOS_BODEGA)
            {
                return BadRequest();
            }

            db.Entry(pRODUCTOS_BODEGA).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PRODUCTOS_BODEGAExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PRODUCTOS_BODEGA
        [ResponseType(typeof(PRODUCTOS_BODEGA))]
        public IHttpActionResult PostPRODUCTOS_BODEGA(PRODUCTOS_BODEGA pRODUCTOS_BODEGA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PRODUCTOS_BODEGA.Add(pRODUCTOS_BODEGA);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PRODUCTOS_BODEGAExists(pRODUCTOS_BODEGA.ID_PRODUCTOS_BODEGA))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pRODUCTOS_BODEGA.ID_PRODUCTOS_BODEGA }, pRODUCTOS_BODEGA);
        }

        // DELETE: api/PRODUCTOS_BODEGA/5
        [ResponseType(typeof(PRODUCTOS_BODEGA))]
        public IHttpActionResult DeletePRODUCTOS_BODEGA(decimal id)
        {
            PRODUCTOS_BODEGA pRODUCTOS_BODEGA = db.PRODUCTOS_BODEGA.Find(id);
            if (pRODUCTOS_BODEGA == null)
            {
                return NotFound();
            }

            db.PRODUCTOS_BODEGA.Remove(pRODUCTOS_BODEGA);
            db.SaveChanges();

            return Ok(pRODUCTOS_BODEGA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PRODUCTOS_BODEGAExists(decimal id)
        {
            return db.PRODUCTOS_BODEGA.Count(e => e.ID_PRODUCTOS_BODEGA == id) > 0;
        }
    }
}