﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RESTAURANTEASY_API.Models;

namespace RESTAURANTEASY_API.Controllers
{
    public class CLIENTESController : ApiController
    {
        private Entities db = new Entities();

        [Route("api/UpdateCliente")]
        [HttpPost]
        public IHttpActionResult UpdateBodega(string nombre, string apellidop, string apellidom, string rut, string mail, string telefono, int id)
        {
            var resp = db.SP_UPDATE_CLIENTE(nombre, apellidop, apellidom, rut, mail, telefono, id);
            return Ok(resp);
        }

        [Route("api/CrearCliente")]
        [HttpPost]
        public IHttpActionResult CrearBodega(string nombre, string apellidop, string apellidom, string rut, string mail, string telefono)
        {
            var resp = db.SP_CREAR_CLIENTE(nombre, apellidop, apellidom, rut, mail, telefono);

            return Ok(resp);
        }

        // GET: api/CLIENTES
        public IQueryable<CLIENTES> GetCLIENTES()
        {
            return db.CLIENTES;
        }

        // GET: api/CLIENTES/5
        [ResponseType(typeof(CLIENTES))]
        public IHttpActionResult GetCLIENTES(decimal id)
        {
            CLIENTES cLIENTES = db.CLIENTES.Find(id);
            if (cLIENTES == null)
            {
                return NotFound();
            }

            return Ok(cLIENTES);
        }

        // PUT: api/CLIENTES/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCLIENTES(decimal id, CLIENTES cLIENTES)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cLIENTES.ID_CLIENTE)
            {
                return BadRequest();
            }

            db.Entry(cLIENTES).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CLIENTESExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CLIENTES
        [ResponseType(typeof(CLIENTES))]
        public IHttpActionResult PostCLIENTES(CLIENTES cLIENTES)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CLIENTES.Add(cLIENTES);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CLIENTESExists(cLIENTES.ID_CLIENTE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = cLIENTES.ID_CLIENTE }, cLIENTES);
        }

        // DELETE: api/CLIENTES/5
        [ResponseType(typeof(CLIENTES))]
        public IHttpActionResult DeleteCLIENTES(decimal id)
        {
            CLIENTES cLIENTES = db.CLIENTES.Find(id);
            if (cLIENTES == null)
            {
                return NotFound();
            }

            db.CLIENTES.Remove(cLIENTES);
            db.SaveChanges();

            return Ok(cLIENTES);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CLIENTESExists(decimal id)
        {
            return db.CLIENTES.Count(e => e.ID_CLIENTE == id) > 0;
        }
    }
}