﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Oracle.ManagedDataAccess.Client;
using RESTAURANTEASY_API.Models;
using System.IO;

namespace RESTAURANTEASY_API.Controllers
{
    public class USUARIOsController : ApiController
    {
        private Entities db = new Entities();

        //[ResponseType(typeof(USUARIO))]
        [Route("api/ExisteUsuario")]
        [HttpGet]
        public IHttpActionResult LoginUsuario(string rut)
        {
            ObjectParameter usu = new ObjectParameter("cONT_USU", typeof(string));
            db.SP_EXISTE_USUARIO(rut, usu);
            int id_usuario = int.Parse(usu.Value.ToString());

            return GetUSUARIO(id_usuario.ToString()); 
        }

        [Route("api/UpdatePassword")]
        [HttpPost]
        public IHttpActionResult UpdatePassword(int id, string password)
        {
            var resp = db.SP_CHANGE_PASSWORD(id, password);
            return Ok(resp);
        }

        [Route("api/CrearUsuario")]
        [HttpPost]
        public IHttpActionResult CrearUsuario(string rut, string name, string phone, string email, string venta, string compra, string finanzas, string bodega, string cocina, string administracion)
        {
            var resp = db.SP_CREAR_USUARIO(rut, name, email, phone, venta, compra, finanzas, bodega, cocina, administracion);
            return Ok(resp);
        }

        [Route("api/UpdateUsuario")]
        [HttpPost]
        public IHttpActionResult UpdateUsuario(string name, string phone, string email, string id, string venta, string compra, string finanzas, string bodega, string cocina, string administracion)
        {
            var resp = db.SP_UPDATE_USER(name, email, phone, id, venta, compra, finanzas, bodega, cocina, administracion);
            return Ok(resp);
        }

        //[ResponseType(typeof(USUARIO))]
        [Route("api/Login")]
        [HttpGet]
        public IHttpActionResult LoginUsuario(string rut, string password)
        {
            ObjectParameter usu = new ObjectParameter("cONT_USU", typeof(string));
            db.SP_LOGIN_USER(rut, password, usu);
            return Ok(int.Parse(usu.Value.ToString()));
        }

        // GET: api/USUARIOs
        public IQueryable<USUARIO> GetUSUARIO()
        {
            return db.USUARIO;
        }

        // GET: api/USUARIOs/5
        [ResponseType(typeof(USUARIO))]
        public IHttpActionResult GetUSUARIO(string id)
        {
            USUARIO uSUARIO = db.USUARIO.Find(id);
            if (uSUARIO == null)
            {
                return NotFound();
            }

            return Ok(uSUARIO);
        }

        // PUT: api/USUARIOs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUSUARIO(string id, USUARIO uSUARIO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != uSUARIO.ID_USUARIO)
            {
                return BadRequest();
            }

            db.Entry(uSUARIO).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!USUARIOExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/USUARIOs
        [ResponseType(typeof(USUARIO))]
        public IHttpActionResult PostUSUARIO(USUARIO uSUARIO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.USUARIO.Add(uSUARIO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (USUARIOExists(uSUARIO.ID_USUARIO))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = uSUARIO.ID_USUARIO }, uSUARIO);
        }

        // DELETE: api/USUARIOs/5
        [ResponseType(typeof(USUARIO))]
        public IHttpActionResult DeleteUSUARIO(string id)
        {
            USUARIO uSUARIO = db.USUARIO.Find(id);
            if (uSUARIO == null)
            {
                return NotFound();
            }

            db.USUARIO.Remove(uSUARIO);
            db.SaveChanges();

            return Ok(uSUARIO);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool USUARIOExists(string id)
        {
            return db.USUARIO.Count(e => e.ID_USUARIO == id) > 0;
        }
    }
}