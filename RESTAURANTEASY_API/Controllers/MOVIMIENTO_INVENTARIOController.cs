﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RESTAURANTEASY_API.Models;

namespace RESTAURANTEASY_API.Controllers
{
    public class MOVIMIENTO_INVENTARIOController : ApiController
    {
        private Entities db = new Entities();

        [Route("api/CrearMovInventario")]
        [HttpPost]
        public IHttpActionResult CrearMovInventario(string compra, string venta, string cliente, string proveedor, DateTime fecha, string producto, string cantidad)
        {
            var resp = db.SP_CREAR_ENTRADA_INVENTARIO(compra, venta, cliente, proveedor, fecha, producto, cantidad);

            return Ok(resp);
        }

        // GET: api/MOVIMIENTO_INVENTARIO
        public IQueryable<MOVIMIENTO_INVENTARIO> GetMOVIMIENTO_INVENTARIO()
        {
            return db.MOVIMIENTO_INVENTARIO;
        }

        // GET: api/MOVIMIENTO_INVENTARIO/5
        [ResponseType(typeof(MOVIMIENTO_INVENTARIO))]
        public IHttpActionResult GetMOVIMIENTO_INVENTARIO(decimal id)
        {
            MOVIMIENTO_INVENTARIO mOVIMIENTO_INVENTARIO = db.MOVIMIENTO_INVENTARIO.Find(id);
            if (mOVIMIENTO_INVENTARIO == null)
            {
                return NotFound();
            }

            return Ok(mOVIMIENTO_INVENTARIO);
        }

        // PUT: api/MOVIMIENTO_INVENTARIO/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMOVIMIENTO_INVENTARIO(decimal id, MOVIMIENTO_INVENTARIO mOVIMIENTO_INVENTARIO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mOVIMIENTO_INVENTARIO.ID)
            {
                return BadRequest();
            }

            db.Entry(mOVIMIENTO_INVENTARIO).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MOVIMIENTO_INVENTARIOExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MOVIMIENTO_INVENTARIO
        [ResponseType(typeof(MOVIMIENTO_INVENTARIO))]
        public IHttpActionResult PostMOVIMIENTO_INVENTARIO(MOVIMIENTO_INVENTARIO mOVIMIENTO_INVENTARIO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MOVIMIENTO_INVENTARIO.Add(mOVIMIENTO_INVENTARIO);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (MOVIMIENTO_INVENTARIOExists(mOVIMIENTO_INVENTARIO.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = mOVIMIENTO_INVENTARIO.ID }, mOVIMIENTO_INVENTARIO);
        }

        // DELETE: api/MOVIMIENTO_INVENTARIO/5
        [ResponseType(typeof(MOVIMIENTO_INVENTARIO))]
        public IHttpActionResult DeleteMOVIMIENTO_INVENTARIO(decimal id)
        {
            MOVIMIENTO_INVENTARIO mOVIMIENTO_INVENTARIO = db.MOVIMIENTO_INVENTARIO.Find(id);
            if (mOVIMIENTO_INVENTARIO == null)
            {
                return NotFound();
            }

            db.MOVIMIENTO_INVENTARIO.Remove(mOVIMIENTO_INVENTARIO);
            db.SaveChanges();

            return Ok(mOVIMIENTO_INVENTARIO);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MOVIMIENTO_INVENTARIOExists(decimal id)
        {
            return db.MOVIMIENTO_INVENTARIO.Count(e => e.ID == id) > 0;
        }
    }
}