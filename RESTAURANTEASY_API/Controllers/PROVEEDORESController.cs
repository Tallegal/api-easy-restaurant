﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RESTAURANTEASY_API.Models;

namespace RESTAURANTEASY_API.Controllers
{
    public class PROVEEDORESController : ApiController
    {
        private Entities db = new Entities();

        [Route("api/UpdateProveedor")]
        [HttpPost]
        public IHttpActionResult UpdateProveedor(string nombre, string rut, string mail, string telefono, string direccion, int id)
        {
            var resp = db.SP_UPDATE_PROVEEDOR(nombre, rut, mail, telefono, direccion, id);
            return Ok(resp);
        }

        [Route("api/CrearProveedor")]
        [HttpPost]
        public IHttpActionResult CrearProveedor(string nombre, string rut, string mail, string telefono, string direccion)
        {
            var resp = db.SP_CREAR_PROVEEDOR(nombre, rut, mail, telefono, direccion);

            return Ok(resp);
        }

        // GET: api/PROVEEDORES
        public IQueryable<PROVEEDORES> GetPROVEEDORES()
        {
            return db.PROVEEDORES;
        }

        // GET: api/PROVEEDORES/5
        [ResponseType(typeof(PROVEEDORES))]
        public IHttpActionResult GetPROVEEDORES(decimal id)
        {
            PROVEEDORES pROVEEDORES = db.PROVEEDORES.Find(id);
            if (pROVEEDORES == null)
            {
                return NotFound();
            }

            return Ok(pROVEEDORES);
        }

        // PUT: api/PROVEEDORES/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPROVEEDORES(decimal id, PROVEEDORES pROVEEDORES)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pROVEEDORES.ID_PROVEEDOR)
            {
                return BadRequest();
            }

            db.Entry(pROVEEDORES).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PROVEEDORESExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PROVEEDORES
        [ResponseType(typeof(PROVEEDORES))]
        public IHttpActionResult PostPROVEEDORES(PROVEEDORES pROVEEDORES)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PROVEEDORES.Add(pROVEEDORES);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PROVEEDORESExists(pROVEEDORES.ID_PROVEEDOR))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pROVEEDORES.ID_PROVEEDOR }, pROVEEDORES);
        }

        // DELETE: api/PROVEEDORES/5
        [ResponseType(typeof(PROVEEDORES))]
        public IHttpActionResult DeletePROVEEDORES(decimal id)
        {
            PROVEEDORES pROVEEDORES = db.PROVEEDORES.Find(id);
            if (pROVEEDORES == null)
            {
                return NotFound();
            }

            db.PROVEEDORES.Remove(pROVEEDORES);
            db.SaveChanges();

            return Ok(pROVEEDORES);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PROVEEDORESExists(decimal id)
        {
            return db.PROVEEDORES.Count(e => e.ID_PROVEEDOR == id) > 0;
        }
    }
}