﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RESTAURANTEASY_API.Models;

namespace RESTAURANTEASY_API.Controllers
{
    public class RESERVAsController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/RESERVAs
        public IQueryable<RESERVA> GetRESERVA()
        {
            return db.RESERVA;
        }

        // GET: api/RESERVAs/5
        [ResponseType(typeof(RESERVA))]
        public IHttpActionResult GetRESERVA(decimal id)
        {
            RESERVA rESERVA = db.RESERVA.Find(id);
            if (rESERVA == null)
            {
                return NotFound();
            }

            return Ok(rESERVA);
        }

        // PUT: api/RESERVAs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRESERVA(decimal id, RESERVA rESERVA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rESERVA.RESERVA_ID)
            {
                return BadRequest();
            }

            db.Entry(rESERVA).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RESERVAExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RESERVAs
        [ResponseType(typeof(RESERVA))]
        public IHttpActionResult PostRESERVA(RESERVA rESERVA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RESERVA.Add(rESERVA);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (RESERVAExists(rESERVA.RESERVA_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = rESERVA.RESERVA_ID }, rESERVA);
        }

        // DELETE: api/RESERVAs/5
        [ResponseType(typeof(RESERVA))]
        public IHttpActionResult DeleteRESERVA(decimal id)
        {
            RESERVA rESERVA = db.RESERVA.Find(id);
            if (rESERVA == null)
            {
                return NotFound();
            }

            db.RESERVA.Remove(rESERVA);
            db.SaveChanges();

            return Ok(rESERVA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RESERVAExists(decimal id)
        {
            return db.RESERVA.Count(e => e.RESERVA_ID == id) > 0;
        }
    }
}