//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RESTAURANTEASY_API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRODUCTOS_MENU
    {
        public decimal ID_PRODUCTO_MENU { get; set; }
        public string NOMBRE_PRODUCTO_MENU { get; set; }
        public string DESCRIPCION_PRODUCTO_MENU { get; set; }
        public string PRECIO_PRODUCTO_MENU { get; set; }
        public string TIPO_PRODUCTO_MENU { get; set; }
    }
}
