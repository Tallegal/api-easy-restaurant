//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RESTAURANTEASY_API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MOVIMIENTO_INVENTARIO
    {
        public decimal ID { get; set; }
        public string COMPRA { get; set; }
        public string VENTA { get; set; }
        public string CLIENTE { get; set; }
        public string PROVEEDOR { get; set; }
        public Nullable<System.DateTime> FECHA { get; set; }
        public string NOMBRE { get; set; }
    }
}
